# Copyright 2019 Creu Blanca
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl.html).
{
    "name": "Base report csv",
    "description": "Port base report csv from odoo 13 to odoo 14",
    "summary": "Base module to create csv report",
    "author": "Mervat Mosaad",
    "website": "mervatmosaad96@gmail.com",
    "category": "Reporting",
    "version": "14.0",
    "license": "",
    "depends": ["base", "web"],
    "data": ["views/webclient_templates.xml"],
    "demo": ["demo/report.xml"],
    "installable": True,
}
